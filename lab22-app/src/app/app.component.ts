import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Site du laboratoire 22';
  maintenant=new Date().toLocaleDateString();
  lien1="assets/images/web.png";
  cours="INF3190";
  
}
